window.addEventListener("load", function() {
  const buttons = document.querySelectorAll(".buttons>button");
  buttons.forEach((button, i) =>
    button.addEventListener("click", function() {
      const modalContents = document.querySelectorAll(".modal-content");
      modalContents.forEach(item => (item.style.display = "none"));
      modalContents[i].style.display = "block";
    })
  );
});
